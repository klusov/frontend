export const sorting: {
  name: string;
  value: 'time' | 'alpha-asc' | 'alpha-desc';
  defaultChecked: boolean;
  text: string;
}[] = [
  {
    name: 'sort',
    value: 'time', // По времени создания
    defaultChecked: true,
    text: 'По времени создания',
  },
  {
    name: 'sort',
    value: 'alpha-asc', // А - Я
    defaultChecked: false,
    text: 'А - Я',
  },
  {
    name: 'sort',
    value: 'alpha-desc', // Я - А
    defaultChecked: false,
    text: 'Я - А',
  },
];
