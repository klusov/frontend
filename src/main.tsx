import React from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom/client';
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import Register from './components/screens/auth/register.tsx';
import Login from './components/screens/auth/login.tsx';
import ErrorPage from './components/screens/error.tsx';
import List from './components/screens/list/list.tsx';
import Tags from './components/screens/tags/tags.tsx';
import Dates from './components/screens/dates/dates.tsx';
import { URLManager } from './config/url.config.ts';
import store from './store/store.ts';
import App from './App.tsx';
import './index.css';

const router = createBrowserRouter([
  {
    path: URLManager.homeURL,
    element: <App />,
    errorElement: <ErrorPage />,
  },
  {
    path: URLManager.loginURL,
    element: <Login />,
  },
  {
    path: URLManager.registerURL,
    element: <Register />,
  },
  {
    path: URLManager.tagsURL,
    element: <Tags />,
  },
  {
    path: URLManager.datesURL,
    element: <Dates />,
  },
  {
    path: URLManager.list(':id'),
    element: <List />,
  },
]);

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>,
);
