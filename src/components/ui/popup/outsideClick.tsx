import { FC, Dispatch, SetStateAction } from 'react';
import { useOutsideClick } from '@/shared/hooks';

export const ClickOutside: FC<{
  children: React.ReactNode;
  isShow: boolean;
  setIsShow: Dispatch<SetStateAction<boolean>> | (() => void);
  className?: string;
}> = ({ children, isShow, setIsShow, className }) => {
  const refShow = useOutsideClick(() => setIsShow(false));
  if (!isShow) return null;
  return (
    <div ref={refShow} className={className}>
      {children}
    </div>
  );
};
