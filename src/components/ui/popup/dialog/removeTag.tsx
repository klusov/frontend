import Dialog from './dialog';

interface DialogProps {
  isOpened: boolean;
  onClose: () => void;
  onOk: () => void;
}

const RemoveTag = ({ isOpened, onClose, onOk }: DialogProps) => {
  return (
    <Dialog
      isOpened={isOpened}
      onClose={onClose}
      header="Удаление тега"
      text="Вы уверены, что хотите удалить тег?"
      primaryButtonText="Удалить"
      primaryButtonOnClick={onOk}
      secondaryButtonText="Отмена"
      secondaryButtonOnClick={onClose}
    />
  );
};

export default RemoveTag;
