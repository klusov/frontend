import Dialog from './dialog';

interface DialogProps {
  isOpened: boolean;
  onClose: () => void;
  onOk: () => void;
}

const RemoveList = ({ isOpened, onClose, onOk }: DialogProps) => {
  return (
    <Dialog
      isOpened={isOpened}
      onClose={onClose}
      header="Удаление списка"
      text="Вы уверены, что хотите удалить список?"
      primaryButtonText="Удалить"
      primaryButtonOnClick={onOk}
      secondaryButtonText="Отмена"
      secondaryButtonOnClick={onClose}
    />
  );
};

export default RemoveList;
