import { ChangeEvent, Dispatch, SetStateAction, useState } from 'react';
import styles from './editableTextItem.module.css';

interface EditableTextItemProps {
  initialText: string;
  isEditMode?: boolean;
  setIsEditingMode?: Dispatch<SetStateAction<boolean>>;
  onSave: (newText: string) => void;
}

const EditableTextItem = ({
  initialText,
  isEditMode,
  setIsEditingMode,
  onSave,
}: EditableTextItemProps) => {
  const [isEditing, setIsEditing] = useState(isEditMode);
  const [text, setText] = useState(initialText);

  const handleDoubleClick = () => {
    setIsEditingMode && setIsEditingMode(true);
    !setIsEditingMode && setIsEditing(true);
  };

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setText(event.target.value);
  };

  const handleBlur = () => {
    setIsEditingMode && setIsEditingMode(false);
    !setIsEditingMode && setIsEditing(false);
    onSave(text);
  };

  const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.code === 'Enter') {
      event.preventDefault();
      event.currentTarget.blur();
    }
  };

  return (
    <div onDoubleClick={handleDoubleClick}>
      {isEditMode || isEditing ? (
        <input
          type="text"
          value={text}
          onChange={handleChange}
          onBlur={handleBlur}
          onKeyPress={handleKeyPress}
          className={styles.input}
          autoFocus={true}
        />
      ) : (
        <>{text}</>
      )}
    </div>
  );
};

export default EditableTextItem;
