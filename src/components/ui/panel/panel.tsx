import { IoCloseOutline } from 'react-icons/io5';
import { PanelProps } from './types';
import styles from './panel.module.css';

const Panel = ({ title, onClose, isOpen, children }: PanelProps) => {
  if (!isOpen) return null;
  return (
    <div className={styles.panel}>
      <div className={styles.panelHeader}>
        <div>{title}</div>
        <IoCloseOutline onClick={onClose} />
      </div>
      {children}
    </div>
  );
};

export default Panel;
