import { ReactNode } from 'react';

export interface PanelProps {
  title: string;
  onClose: () => void;
  isOpen: boolean;
  children: ReactNode;
}
