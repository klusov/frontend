import {
  Dispatch,
  InputHTMLAttributes,
  SetStateAction,
  forwardRef,
} from 'react';
import { FieldError } from 'react-hook-form';
import { useState } from 'react';
import { AiOutlineEye, AiOutlineEyeInvisible } from 'react-icons/ai';
import styles from './input.module.css';

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  label: string;
  error: FieldError | undefined;
  errorMessage: string;
  setPasswordView?: Dispatch<SetStateAction<boolean>>;
  withEye?: boolean;
}

const Input = forwardRef<HTMLInputElement, InputProps>(
  (
    {
      label,
      name,
      type = 'text',
      error,
      errorMessage,
      className = '',
      withEye,
      ...rest
    },
    ref,
  ) => {
    const [passwordView, setPasswordView] = useState(false);

    return (
      <div className={className}>
        <label htmlFor={name} className={styles.label}>
          {label}
        </label>

        {error && <span className={styles.errorMsg}>{errorMessage}</span>}

        <div className={styles.inputContainer}>
          <input
            {...rest}
            type={passwordView ? 'text' : type}
            name={name}
            ref={ref}
            className={styles.input}
          />
          {withEye && (
            <div
              className={styles.eye}
              onClick={() => setPasswordView(!passwordView)}
            >
              {passwordView ? <AiOutlineEye /> : <AiOutlineEyeInvisible />}
            </div>
          )}
        </div>
      </div>
    );
  },
);

Input.displayName = 'Input';

export default Input;
