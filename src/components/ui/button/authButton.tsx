import styles from './button.module.css';

const AuthButton = ({ text }: { text: string }) => {
  return (
    <button className={styles.authButton} type="submit">
      {text}
    </button>
  );
};

export default AuthButton;
