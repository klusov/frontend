import cn from 'classnames';
import { ButtonProps } from './types';
import styles from './button.module.css';

const SecondaryButton = ({
  text,
  onClick,
  className,
  ...props
}: ButtonProps) => {
  return (
    <button
      className={cn(styles.button, styles.secondaryButton, className)}
      onClick={onClick}
      {...props}
    >
      {text}
    </button>
  );
};

export default SecondaryButton;
