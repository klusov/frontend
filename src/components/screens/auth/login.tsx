import { FC } from 'react';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { Login } from '@/shared/types';
import Logo from '@/components/ui/logo/logo';
import Input from '@/components/ui/input/input';
import { URLManager } from '@/config/url.config';
import AuthButton from '@/components/ui/button/authButton';
import styles from './auth.module.css';

const Login: FC = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Login>();

  const auth = { error: '' };

  const onSubmit = handleSubmit(data => {
    console.log(data);
  });

  return (
    <>
      <div className={styles.body}>
        <form className={styles.form} onSubmit={onSubmit}>
          <Logo className={styles.logo} />

          <Input
            label="Почта"
            error={errors.email}
            errorMessage="Введите почту"
            {...register('email', { required: true })}
            type="email"
            id="email"
            className={styles.formItem}
          />

          <Input
            label="Пароль"
            error={errors.password}
            errorMessage="Введите пароль"
            id="password"
            {...register('password', { required: true })}
            type={'password'}
            className={styles.formItem}
            withEye
          />

          {auth.error && (
            <span className={styles.errorMsgAuth}>
              Мы вас не узнали. Проверьте, правильно ли вы указали почту и
              пароль
            </span>
          )}

          <div className={styles.forgotPswBtn}>
            <div className={styles.forgotPassword}>Забыли пароль?</div>
            <AuthButton text="Войти" />
          </div>

          <div className={styles.addAccount}>
            Нет аккаунта?&nbsp;
            <Link to={URLManager.registerURL}>Зарегистрируйтесь</Link>
          </div>
        </form>
      </div>
    </>
  );
};

export default Login;
