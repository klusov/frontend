import { FC } from 'react';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { Register } from '@/shared/types';
import Logo from '@/components/ui/logo/logo';
import Input from '@/components/ui/input/input';
import { URLManager } from '@/config/url.config';
import AuthButton from '@/components/ui/button/authButton';
import styles from './auth.module.css';

const Register: FC = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
  } = useForm<Register>();

  const auth = { error: '' };

  const onSubmit = handleSubmit(data => {
    console.log(data);
  });

  return (
    <>
      <div className={styles.body}>
        <form className={styles.form} onSubmit={onSubmit}>
          <Logo className={styles.logo} />

          <Input
            label="Почта"
            error={errors.email}
            errorMessage="Введите почту"
            {...register('email', { required: true })}
            type="email"
            id="email"
            className={styles.formItem}
          />

          <Input
            label="Пароль"
            error={errors.password}
            errorMessage="Введите пароль"
            id="password"
            {...register('password', { required: true })}
            type={'password'}
            className={styles.formItem}
            withEye
          />

          <Input
            label="Повторите пароль"
            error={errors.confirmPassword}
            errorMessage="Пароли не совпадают"
            id="confirmPassword"
            {...register('confirmPassword', {
              required: true,
              validate: value => value === watch('password'),
            })}
            type={'password'}
            className={styles.formItem}
            withEye
          />

          {auth.error && (
            <span className={styles.errorMsgAuth}>
              Пользователь с такой почтой уже зарегистрирован
            </span>
          )}

          <AuthButton text="Зарегистрироваться" />

          <div className={styles.addAccount}>
            Есть аккаунт? <Link to={URLManager.loginURL}>Войдите</Link>
          </div>
        </form>
      </div>
    </>
  );
};

export default Register;
