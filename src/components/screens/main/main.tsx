import React, { useState } from 'react';
import { IoIosArrowDown } from 'react-icons/io';
import { AiOutlinePlus } from 'react-icons/ai';
import { useNavigate } from 'react-router-dom';
import PrimaryButton from '@/components/ui/button/primaryButton';
import { useCreateListMutation } from '@/services/list.service';
import Layout from '@/components/layout/layout';
import { ClickOutside } from '@/components/ui/popup/outsideClick';
import RadioInput from '@/components/ui/input/radio';
import { sorting } from '@/data/sort.data';
import { URLManager } from '@/config/url.config';
import styles from './main.module.css';
import Table from './table/table';

const Main = () => {
  const [isArchivedList, setIsArchivedList] = useState(false);
  const [isSortDropdownOpen, setIsSortDropdownOpen] = useState(false);
  const [selectedSortValue, setSelectedSortValue] = useState<string>('time');

  const navigate = useNavigate();

  const toggleSortDropdown = () => {
    setIsSortDropdownOpen(!isSortDropdownOpen);
  };

  const handleSortChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSelectedSortValue(event.target.value);
  };

  const [createList] = useCreateListMutation();
  const handleCreateNewList = async () => {
    const newList = await createList({
      title: 'Название нового списка',
      parentId: null,
    });
    if ('data' in newList) {
      navigate(URLManager.list(newList.data.id));
    } else {
      console.error(newList.error);
    }
  };

  return (
    <Layout>
      <div className={styles.header}>
        <div className={styles.links}>
          <a
            onClick={() => setIsArchivedList(false)}
            className={!isArchivedList ? styles.active : ''}
          >
            Активные&nbsp;списки
          </a>
          <a
            onClick={() => setIsArchivedList(true)}
            className={isArchivedList ? styles.active : ''}
          >
            Архив
          </a>
        </div>
        <div className={styles.buttons}>
          <div className={styles.sort} onClick={toggleSortDropdown}>
            Сортировать <IoIosArrowDown />
            <ClickOutside
              isShow={isSortDropdownOpen}
              setIsShow={setIsSortDropdownOpen}
              className={styles.tooltip}
            >
              {sorting.map((option, index) => (
                <RadioInput
                  label={option.text}
                  type="radio"
                  name={option.name}
                  value={option.value}
                  checked={selectedSortValue === option.value}
                  onChange={handleSortChange}
                  key={index}
                />
              ))}
            </ClickOutside>
          </div>

          <PrimaryButton
            text="Создать"
            icon={<AiOutlinePlus />}
            className={styles.button}
            onClick={handleCreateNewList}
          />
        </div>
      </div>

      <Table
        listIsDoneParam={isArchivedList}
        selectedSortValue={selectedSortValue}
      />
    </Layout>
  );
};

export default Main;
