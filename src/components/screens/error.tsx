import { useRouteError } from 'react-router-dom';

export default function ErrorPage() {
  const error = useRouteError() as Error;

  return (
    <div id="error-page">
      <h1>Страница не найдена :(</h1>
      <p>Извините, произошла непредвиденная ошибка</p>
      <p>
        <i>{error.name || error.message}</i>
      </p>
    </div>
  );
}
