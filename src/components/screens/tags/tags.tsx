import React, { useState } from 'react';
import { IoIosArrowDown } from 'react-icons/io';
import Layout from '@/components/layout/layout';
import { ClickOutside } from '@/components/ui/popup/outsideClick';
import RadioInput from '@/components/ui/input/radio';
import styles from './tags.module.css';
import { tagAPI } from '@/services/tag.service';
import RenderTagItem from './renderTagItem';

const Tags = () => {
  const [isSortDropdownOpen, setIsSortDropdownOpen] = useState(false);
  const [selectedSortValue, setSelectedSortValue] = useState<string>('popular');

  const { data: tags } = tagAPI.useGetAllTagsQuery();

  const toggleSortDropdown = () => {
    setIsSortDropdownOpen(!isSortDropdownOpen);
  };

  const handleSortChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSelectedSortValue(event.target.value);
  };

  const sortedTags = (() => {
    switch (selectedSortValue) {
      case 'popular':
        return (
          tags &&
          tags.toSorted((a, b) => {
            const lengthA = a.lists ? a.lists.length : 0;
            const lengthB = b.lists ? b.lists.length : 0;
            return lengthB - lengthA;
          })
        );
      case 'alpha-asc':
        return tags && tags.toSorted((a, b) => a.title.localeCompare(b.title));
      case 'alpha-desc':
        return tags && tags.toSorted((a, b) => b.title.localeCompare(a.title));
      default:
        return tags;
    }
  })();

  return (
    <Layout>
      <div className={styles.header}>
        <div className={styles.links}>
          <span className={styles.active}>Все&nbsp;теги</span>
          <span className={styles.timing}>Временные</span>
        </div>

        <div className={styles.sorting}>
          <div className={styles.sortContainer} onClick={toggleSortDropdown}>
            Сортировать <IoIosArrowDown />
            <ClickOutside
              isShow={isSortDropdownOpen}
              setIsShow={setIsSortDropdownOpen}
              className={styles.sortTooltip}
            >
              <RadioInput
                label="По&nbsp;популярности"
                type="radio"
                name="sort"
                value="popular"
                checked={selectedSortValue === 'popular'}
                onChange={handleSortChange}
              />
              <RadioInput
                label="А - Я"
                type="radio"
                name="sort"
                value="alpha-asc"
                checked={selectedSortValue === 'alpha-asc'}
                onChange={handleSortChange}
              />
              <RadioInput
                label="Я - А"
                type="radio"
                name="sort"
                value="alpha-desc"
                checked={selectedSortValue === 'alpha-desc'}
                onChange={handleSortChange}
              />
            </ClickOutside>
          </div>
        </div>
      </div>

      <div className={styles.tagsWrapper}>
        {sortedTags &&
          sortedTags.map(tag => <RenderTagItem tag={tag} key={tag.id} />)}
      </div>
    </Layout>
  );
};

export default Tags;
