import { useState } from 'react';
import { LuSettings } from 'react-icons/lu';
import { MdOutlineKeyboard } from 'react-icons/md';
import cn from 'classnames';
import { useParams } from 'react-router-dom';
import Layout from '@/components/layout/layout';
import {
  setHideCompleted,
  setMoveCompletedDown,
  setShowDetails,
  setShowProgressCounter,
  setListStyle,
} from '@/store/slices/list.slice';
import styles from './list.module.css';
import TreeList from './treeList';
import ProgressBar from './progressBar/progressBar';
import Panel from '@/components/ui/panel/panel';
import {
  listAPI,
  useCreateListMutation,
  useUpdateListMutation,
} from '@/services/list.service';
import EditableTextItem from '@/components/ui/editableTextItem/editableTextItem';
import { useAppDispatch, useAppSelector } from '@/shared/hooks';
import { handleKeyPress } from '@/shared/utils/keypress.utils';
import Breadcrumbs from './breadcrumbs';

const List = () => {
  const { id } = useParams();
  const {
    data: list,
    isLoading,
    error,
  } = listAPI.useGetListByIdQuery({ id: id || '' });

  const [isOpenOption, setIsOpenOption] = useState(true);
  const [isOpenHotkey, setIsOpenHotkey] = useState(true);
  const [isSubItem, setIsSubItem] = useState(false);
  const [textareaValue, setTextareaValue] = useState('');
  const [showNewItem, setShowNewItem] = useState(list?.children.length === 0);

  const { hideCompleted, showDetails, showProgressCounter, listStyle } =
    useAppSelector(state => state.lists.options);

  const dispatch = useAppDispatch();
  const [createList] = useCreateListMutation();
  const [updateList] = useUpdateListMutation();

  const handleCheckboxChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, checked } = event.target;

    switch (name) {
      case 'hideCompleted':
        dispatch(setHideCompleted(checked));
        break;
      case 'moveCompletedDown':
        dispatch(setMoveCompletedDown(checked));
        break;
      case 'showDetails':
        dispatch(setShowDetails(checked));
        break;
      case 'showProgressCounter':
        dispatch(setShowProgressCounter(checked));
        break;
      default:
        break;
    }
  };

  const handleSelectChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const { value } = event.target;
    dispatch(setListStyle(value));
  };

  const toggleOption = (option: 'option' | 'hotkeys') => {
    if (option === 'option') {
      setIsOpenOption(!isOpenOption);
    } else if (option === 'hotkeys') {
      setIsOpenHotkey(!isOpenHotkey);
    }
  };

  const handleTitleChange = (newTitle: string) => {
    if (list && newTitle !== list.title) {
      updateList({ id: list.id, title: newTitle });
    }
  };

  const handleTextareaChange = (
    event: React.ChangeEvent<HTMLTextAreaElement>,
  ) => {
    setTextareaValue(event.target.value);
  };

  const handleKeyDown = (
    event: React.KeyboardEvent<HTMLDivElement>,
    option: 'option' | 'hotkeys',
  ) => {
    if (event.key === ' ') {
      toggleOption(option);
    }
  };

  const postNewListItem = () => {
    if (!textareaValue.trim()) {
      return;
    }

    if (list) {
      createList({
        title: textareaValue,
        parentId: list.id,
      });
    }

    setTextareaValue('');
  };

  const handleKeyPressWrapper = (event: React.KeyboardEvent) => {
    handleKeyPress({
      event,
      postNewListItem,
      setShowNewItem,
      setIsSubItem,
    });
  };

  return (
    <Layout>
      {id && <Breadcrumbs id={id} />}
      <div className={styles.header}>
        <div className={styles.listTitle} tabIndex={0}>
          {list && (
            <EditableTextItem
              initialText={list.title}
              onSave={handleTitleChange}
              isEditMode={list.children.length === 0}
            />
          )}
        </div>
        <div className={styles.options}>
          {showProgressCounter && <ProgressBar value={0} max={0} />}
          <div
            className={styles.option}
            tabIndex={0}
            onKeyDown={event => handleKeyDown(event, 'option')}
          >
            <input
              type="checkbox"
              name="option"
              id="option"
              checked={isOpenOption}
              onChange={() => setIsOpenOption(!isOpenOption)}
            />
            <label htmlFor="option">
              <LuSettings />
            </label>
          </div>
          <div
            className={styles.option}
            tabIndex={0}
            onKeyDown={event => handleKeyDown(event, 'hotkeys')}
          >
            <input
              type="checkbox"
              name="option"
              id="hotkeys"
              checked={isOpenHotkey}
              onChange={() => setIsOpenHotkey(!isOpenHotkey)}
            />
            <label htmlFor="hotkeys">
              <MdOutlineKeyboard />
            </label>
          </div>
        </div>
      </div>

      <div className={styles.main}>
        <div>
          {isLoading && <span>Загрузка...</span>}
          {error && <span>Произошла ошибка</span>}
          {list &&
            list.children
              .toSorted((a, b) => a.position - b.position)
              .map((item, index) => (
                <TreeList listItem={item} key={item.id} level={++index} />
              ))}
          {(list?.children.length === 0 || showNewItem) && (
            <textarea
              className={cn(styles.textarea, isSubItem && styles.subitem)}
              value={textareaValue}
              onChange={handleTextareaChange}
              onKeyDown={event => handleKeyPressWrapper(event)}
              onBlur={postNewListItem}
            />
          )}
        </div>

        <div className={styles.panels}>
          <Panel
            title="Опции"
            isOpen={isOpenOption}
            onClose={() => setIsOpenOption(!isOpenOption)}
          >
            <div>
              <label>
                <input
                  type="checkbox"
                  name="hideCompleted"
                  checked={hideCompleted}
                  onChange={handleCheckboxChange}
                />
                Скрыть завершенные элементы
              </label>
            </div>
            <div>
              <label>
                <input
                  type="checkbox"
                  name="showDetails"
                  checked={showDetails}
                  onChange={handleCheckboxChange}
                />
                Показать сведения
              </label>
            </div>
            <div>
              <label>
                <input
                  type="checkbox"
                  name="showProgressCounter"
                  checked={showProgressCounter}
                  onChange={handleCheckboxChange}
                />
                Счетчик прогресса
              </label>
            </div>
            <div className={styles.select}>
              <label htmlFor="listStyle">Стиль списка</label>
              <select
                name="listStyle"
                id="listStyle"
                value={listStyle}
                onChange={handleSelectChange}
              >
                <option value="none">None</option>
                <option value="number">Нумерованный</option>
                <option value="checkbox">Чекбокс</option>
              </select>
            </div>
          </Panel>
          <Panel
            title="Горячие клавиши"
            isOpen={isOpenHotkey}
            onClose={() => setIsOpenHotkey(!isOpenHotkey)}
          >
            <div className={styles.hotkeyLabel}>
              <span className={styles.hotkeyCommand}>Enter</span>
              Добавить элемент ниже
            </div>
            <div className={styles.hotkeyLabel}>
              <span className={styles.hotkeyCommand}>Shift + Enter</span>
              Добавить подпункт
            </div>
            <div className={styles.hotkeyLabel}>
              <span className={styles.hotkeyCommand}>Пробел</span>
              Закрыть / открыть задачу
            </div>
            <div className={styles.hotkeyLabel}>
              <span className={styles.hotkeyCommand}>Tab</span>
              Перейти к следующему
            </div>
            <div className={styles.hotkeyLabel}>
              <span className={styles.hotkeyCommand}>Shift + Tab</span>
              Перейти к предыдущему
            </div>
            <div className={styles.hotkeyLabel}>
              <span className={styles.hotkeyCommand}>Delete</span>
              Удалить элемент
            </div>
          </Panel>
        </div>
      </div>
    </Layout>
  );
};

export default List;
