import { useEffect, useState } from 'react';
import { GroupBase, MultiValue, StylesConfig, Theme } from 'react-select';
import CreatableSelect from 'react-select/creatable';
import { getContrastColor } from '@/shared/utils/getContrastColor.utils';
import {
  useCreateTagMutation,
  useGetAllTagsQuery,
} from '@/services/tag.service';
import {
  useAddTagToListMutation,
  useGetTagsByListIdQuery,
  useRemoveTagFromListMutation,
} from '@/services/list.service';
import styles from './menu.module.css';

interface Option {
  id: string;
  value: string;
  label: string;
  color: string;
}

const TagSelect = ({ parentId }: { parentId: string }) => {
  const [options, setOptions] = useState<Option[]>([]);
  const { data: tags = [], isLoading } = useGetAllTagsQuery();
  const { data: listTags = [] } = useGetTagsByListIdQuery({ id: parentId });
  const [createTag] = useCreateTagMutation();
  const [addTagToList] = useAddTagToListMutation();
  const [removeTagFromList] = useRemoveTagFromListMutation();
  const [selectedOptions, setSelectedOptions] = useState<MultiValue<Option>>(
    [],
  );

  const listTagsOptions: Option[] = listTags.map(tag => ({
    value: tag.title,
    label: tag.title,
    ...tag,
  }));

  useEffect(() => {
    if (tags) {
      const updatedOptions: Option[] = tags.map(tag => ({
        value: tag.title,
        label: tag.title,
        ...tag,
      }));
      setOptions(updatedOptions);
    }
  }, [tags]);

  useEffect(() => {
    if (listTags) {
      const updatedListTagsOptions: Option[] = listTags.map(tag => ({
        value: tag.title,
        label: tag.title,
        ...tag,
      }));
      setSelectedOptions(updatedListTagsOptions);
    }
  }, [listTags]);

  const handleChange = (newValue: MultiValue<Option>) => {
    const removedTags = selectedOptions.filter(
      option => !newValue.includes(option),
    );
    removedTags.forEach(tag => handleRemoveTag(parentId, tag.id));

    setSelectedOptions(newValue);
    newValue.forEach(tag => handleAddTag(parentId, tag.id));
  };

  const handleCreateOption = async (inputValue: string) => {
    const response = await createTag({ title: inputValue });
    if ('data' in response) {
      const newTag = response.data;
      const newOption = {
        value: newTag.title,
        label: newTag.title,
        ...newTag,
      };
      setOptions(prevOptions => [...prevOptions, newOption]);
      setSelectedOptions(prevOptions => [...prevOptions, newOption]);
      handleAddTag(parentId, newTag.id);
    }
  };

  const handleRemoveTag = async (listId: string, tagId: string) => {
    try {
      await removeTagFromList({ listId, tagId });
    } catch (error) {
      console.error('Error removing tag:', error);
    }
  };

  const handleAddTag = async (listId: string, tagId: string) => {
    try {
      await addTagToList({ listId, tagId });
    } catch (err) {
      console.error('Error adding tag:', err);
    }
  };

  return (
    <div className={styles.tooltip}>
      <CreatableSelect
        isMulti
        defaultMenuIsOpen
        options={options}
        defaultValue={listTagsOptions}
        placeholder={'Выберите тег'}
        styles={customStyles}
        theme={customTheme}
        onChange={handleChange}
        onCreateOption={handleCreateOption}
        isLoading={isLoading}
        value={selectedOptions}
      />
    </div>
  );
};

export default TagSelect;

const customStyles: StylesConfig<Option, true, GroupBase<Option>> = {
  control: baseStyles => ({ ...baseStyles, borderColor: 'transparent' }),
  container: baseStyles => ({ ...baseStyles, flex: '1' }),
  placeholder: baseStyles => ({ ...baseStyles, padding: '0 8px' }),
  indicatorsContainer: () => ({ display: 'none' }),
  multiValue: (base, state) => ({
    ...base,
    backgroundColor: state.data.color,
    color: getContrastColor(state.data.color),
  }),
  multiValueLabel: (base, state) => ({
    ...base,
    backgroundColor: state.data.color,
    color: getContrastColor(state.data.color),
  }),
};

const customTheme = (theme: Theme) => ({
  ...theme,
  colors: {
    ...theme.colors,
    primary: 'transparent',
    primary50: '#0088bb', // :active
    primary25: '#00bbee', // :hover
    neutral0: '#f5f5f5', // bgc CreatableSelect
    neutral30: 'transparent',
  },
});
