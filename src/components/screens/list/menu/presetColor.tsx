import { useUpdateListMutation } from '@/services/list.service';
import styles from './menu.module.css';

const PresetColor = ({ id }: { id: string }) => {
  const [updateList] = useUpdateListMutation();

  const handleClick = (index: number) => {
    updateList({
      id,
      color: colors[index],
    });
  };

  const colors = [
    '#ffffff', // var(--background-white)
    '#ffe9e6', // var(--red-00)
    '#ffe0c3', // var(--orange-00)
    '#ffefd4', // var(--yellow-00)
    '#d7f8ae', // var(--green-00)
    '#dde6ff', // var(--blue-00)
    '#f7d7ff', // var(--purple-00)
    '#e8e9eb', // var(--background-dark-gray)
    '#f5f5f5', // var(--background-gray)
  ];

  return (
    <div className={styles.presetColorWrapper}>
      <div className={styles.presetColor} onClick={() => handleClick(0)}>
        0
      </div>
      <div className={styles.presetColor} onClick={() => handleClick(1)}>
        1
      </div>
      <div className={styles.presetColor} onClick={() => handleClick(2)}>
        2
      </div>
      <div className={styles.presetColor} onClick={() => handleClick(3)}>
        3
      </div>
      <div className={styles.presetColor} onClick={() => handleClick(4)}>
        4
      </div>
      <div className={styles.presetColor} onClick={() => handleClick(5)}>
        5
      </div>
      <div className={styles.presetColor} onClick={() => handleClick(6)}>
        6
      </div>
      <div className={styles.presetColor} onClick={() => handleClick(7)}>
        7
      </div>
      <div className={styles.presetColor} onClick={() => handleClick(8)}>
        8
      </div>
    </div>
  );
};

export default PresetColor;
