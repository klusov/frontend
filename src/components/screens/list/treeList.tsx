import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { VscChevronDown, VscChevronRight } from 'react-icons/vsc';
import { BsThreeDotsVertical } from 'react-icons/bs';
import cn from 'classnames';
import EditableTextItem from '@/components/ui/editableTextItem/editableTextItem';
import { getContrastColor } from '@/shared/utils/getContrastColor.utils';
import { formatDateToLocale } from '@/shared/utils/datetime.utils';
import { handleKeyPress } from '@/shared/utils/keypress.utils';
import { useAppSelector } from '@/shared/hooks';
import {
  useCreateListMutation,
  useDeleteListMutation,
  useUpdateListMutation,
} from '@/services/list.service';
import { TreeListProps } from './types';
import Menu from './menu/menu';
import styles from './list.module.css';

const TreeList: React.FC<TreeListProps> = ({
  listItem,
  level,
  parentPath,
  showChildren = true,
}) => {
  const [show, setShow] = useState(showChildren);
  const [showMenu, setShowMenu] = useState(false);
  const [showNewItem, setShowNewItem] = useState(false);
  const [showManageTag, setShowManageTag] = useState(false);
  const [showManageColor, setShowManageColor] = useState(false);
  const [showPresetDate, setShowPresetDate] = useState(false);
  const [isSubItem, setIsSubItem] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [textareaValue, setTextareaValue] = useState('');

  const [createList] = useCreateListMutation();
  const [updateList] = useUpdateListMutation();
  const [deleteList] = useDeleteListMutation();

  const navigate = useNavigate();

  const { hideCompleted, listStyle, showDetails } = useAppSelector(
    state => state.lists.options,
  );

  const itemPath =
    parentPath === undefined ? String(level) : `${parentPath}.${level}`;

  const toggleItemActive = () => {
    !isEditing &&
      !showNewItem &&
      !showMenu &&
      updateList({ id: listItem.id, isDone: !listItem.isDone });
  };

  const handleTitleChange = (newTitle: string) => {
    if (listItem && newTitle !== listItem.title) {
      updateList({ id: listItem.id, title: newTitle });
    }
  };

  const handleTextareaChange = (
    event: React.ChangeEvent<HTMLTextAreaElement>,
  ) => {
    setTextareaValue(event.target.value);
  };

  const postNewListItem = () => {
    if (!textareaValue.trim()) {
      setShowNewItem(false);
      return;
    }

    if (isSubItem) {
      createList({
        title: textareaValue,
        parentId: listItem.id,
        position: listItem.position + 1,
      });
    } else {
      createList({
        title: textareaValue,
        parentId: listItem.parentId,
        position: listItem.position + 1,
      });
    }

    setTextareaValue('');
  };

  const handleDeleteItem = () => {
    deleteList(listItem.id);
  };

  const handleKeyPressWrapper = (event: React.KeyboardEvent) => {
    handleKeyPress({
      event,
      toggleItemActive,
      postNewListItem,
      setShowNewItem,
      setShowMenu,
      setIsSubItem,
      setIsEditing,
      handleDeleteItem,
      setShowManageTag,
    });
  };

  const focusOnList = () => {
    navigate(`/list/${listItem.id}`);
  };

  useEffect(() => {
    setShowManageTag(false);
    setShowPresetDate(false);
  }, [showMenu]);

  const fillCoor = {
    backgroundColor: listItem.color || '#fff',
  };

  if (hideCompleted && listItem.isDone) return null;

  return (
    <div className={styles.treeListItem}>
      <div
        className={cn(styles.taskItem, listItem.isDone && styles.doneTask)}
        tabIndex={0}
        onKeyDown={handleKeyPressWrapper}
        style={fillCoor}
      >
        {listItem.children.length > 0 && (
          <span className={styles.handleShow} onClick={() => setShow(!show)}>
            {show ? <VscChevronDown /> : <VscChevronRight />}
          </span>
        )}

        {listStyle === 'number' && <div>{itemPath}</div>}
        {listStyle === 'checkbox' && (
          <input
            type="checkbox"
            checked={listItem.isDone}
            onChange={toggleItemActive}
            className={styles.status}
            tabIndex={-1}
          />
        )}
        <EditableTextItem
          initialText={listItem.title}
          onSave={handleTitleChange}
          isEditMode={isEditing}
          setIsEditingMode={setIsEditing}
        />
        {listItem.tags.map(tag => (
          <div
            className={styles.tagItem}
            key={tag.id}
            style={{
              backgroundColor: tag.color,
              color: getContrastColor(tag.color),
            }}
          >
            #{tag.title}
          </div>
        ))}
        {!showPresetDate && listItem.presetDate && (
          <span className={styles.presetDate}>
            {formatDateToLocale(listItem.presetDate)}
          </span>
        )}
        {showDetails && (
          <span className={styles.details}>
            {formatDateToLocale(listItem.updateDate)}
          </span>
        )}
        <BsThreeDotsVertical
          className={styles.treeDots}
          onClick={() => setShowMenu(!showMenu)}
        />
        <Menu
          parentList={listItem}
          showMenu={showMenu}
          setShowMenu={setShowMenu}
          showManageTag={showManageTag}
          setShowManageTag={setShowManageTag}
          showManageColor={showManageColor}
          setShowManageColor={setShowManageColor}
          showPresetDate={showPresetDate}
          setShowPresetDate={setShowPresetDate}
          setShowNewItem={setShowNewItem}
          setIsSubItem={setIsSubItem}
          handleDeleteItem={handleDeleteItem}
          focusOnList={focusOnList}
          setIsEditing={setIsEditing}
        />
      </div>
      {show && listItem.children.length > 0 && (
        <div className={styles.childrenContainer}>
          {listItem.children
            .toSorted((a, b) => a.position - b.position)
            .map((child, index) => (
              <TreeList
                key={child.id}
                listItem={child}
                level={index + 1}
                parentPath={itemPath}
              />
            ))}
        </div>
      )}
      {showNewItem && (
        <textarea
          className={cn(styles.textarea, isSubItem && styles.subitem)}
          autoFocus={showNewItem}
          value={textareaValue}
          onChange={handleTextareaChange}
          onKeyDown={handleKeyPressWrapper}
          onBlur={postNewListItem}
        />
      )}
    </div>
  );
};

export default TreeList;
