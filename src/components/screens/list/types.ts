import { ListItem } from '@/shared/types';

export interface TreeListProps {
  listItem: ListItem;
  level: number;
  parentPath?: string;
  showChildren?: boolean;
}
