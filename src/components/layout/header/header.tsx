import React, { useState } from 'react';
import { LuSettings2 } from 'react-icons/lu';
import { NavLink, useNavigate } from 'react-router-dom';
import SecondaryButton from '@/components/ui/button/secondaryButton';
import Logo from '@/components/ui/logo/logo';
import { URLManager } from '@/config/url.config';
import { useGetListsQuery } from '@/services/list.service';
import styles from './header.module.css';

const Header = () => {
  const [searchQuery, setSearchQuery] = useState<string>('');
  const navigate = useNavigate();
  const {
    data: lists,
    error,
    isLoading,
  } = useGetListsQuery({ title: searchQuery });

  const handleSearchInputChange = (
    event: React.ChangeEvent<HTMLInputElement>,
  ) => {
    setSearchQuery(event.target.value);
  };

  return (
    <div className={styles.header}>
      <Logo />

      <div className={styles.search}>
        <input
          type="text"
          value={searchQuery}
          onChange={handleSearchInputChange}
          placeholder="Поиск..."
        />
        <LuSettings2 />
        {searchQuery && (
          <div className={styles.searchResult}>
            {isLoading && <div>Загрузка...</div>}
            {error && <div>Ошибка загрузки списков.</div>}
            {lists?.map(list => <div key={list.id}>{list.title}</div>)}
            {!isLoading && !lists?.length && <div>Не найдено...</div>}
          </div>
        )}
      </div>

      <div className={styles.links}>
        <NavLink
          to={URLManager.homeURL}
          className={({ isActive }) => (isActive ? styles.active : '')}
          tabIndex={0}
        >
          Списки
        </NavLink>
        <NavLink
          to={URLManager.datesURL}
          className={({ isActive }) => (isActive ? styles.active : '')}
          tabIndex={0}
        >
          Периоды
        </NavLink>
        <NavLink
          to={URLManager.tagsURL}
          className={({ isActive }) => (isActive ? styles.active : '')}
          tabIndex={0}
        >
          Теги
        </NavLink>
      </div>

      <SecondaryButton
        text="Вход"
        onClick={() => navigate(URLManager.loginURL)}
        className={styles.auth}
        tabIndex={0}
      />
    </div>
  );
};

export default Header;
