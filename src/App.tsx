import Main from './components/screens/main/main';

function App() {
  return (
    <>
      <Main />
    </>
  );
}

export default App;
