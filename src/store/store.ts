import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query/react';
import { listAPI } from '@/services/list.service';
import { tagAPI } from '@/services/tag.service';
import listReducer from './slices/list.slice';

const store = configureStore({
  reducer: {
    lists: listReducer,
    [listAPI.reducerPath]: listAPI.reducer,
    [tagAPI.reducerPath]: tagAPI.reducer,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware().concat(listAPI.middleware, tagAPI.middleware),
});

setupListeners(store.dispatch);

export default store;

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
