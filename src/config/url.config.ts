const URLManager = {
  homeURL: '/',
  loginURL: '/login',
  registerURL: '/register',
  tagsURL: '/tags',
  datesURL: '/dates',
  list: (id: string): string => `/list/${id}`,
};

export { URLManager };
