export interface Login {
  email: string;
  password: string;
}
export interface Register extends Login {
  confirmPassword: string;
}

export interface Tag {
  id: string;
  title: string;
  color: string;
  lists?: ListItem[];
}
export interface TagData {
  title: string;
  color?: string;
}

export interface ListItem {
  id: string;
  title: string;
  isDone: boolean;
  color: string | null;
  tags: Tag[];
  createDate: string;
  updateDate: string;
  presetDate: string | null;
  parentId: string | null;
  children: ListItem[];
  position: number;
  showNewItem?: boolean;
}

export interface ListItemTitleParentId {
  title: string;
  parentId: string | null;
  position?: number;
}

export interface ListState {
  items: ListItem[] | null;
  options: {
    hideCompleted: boolean;
    moveCompletedDown: boolean;
    showDetails: boolean;
    showProgressCounter: boolean;
    listStyle: 'none' | 'number' | 'checkbox';
  };
}
