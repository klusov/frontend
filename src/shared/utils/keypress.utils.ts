interface PressedKey {
  code: string;
  timestamp: number;
}

const pressedKeys: PressedKey[] = [];

function getKeyPress(code: string): string {
  pressedKeys.push({ code, timestamp: Date.now() });

  if (pressedKeys.length > 2) pressedKeys.shift();

  if (pressedKeys.length === 2) {
    const timeDifference = pressedKeys[1].timestamp - pressedKeys[0].timestamp;
    const combinedKeys =
      timeDifference < 300
        ? pressedKeys.map(key => key.code).join('_')
        : pressedKeys[pressedKeys.length - 1].code;
    return combinedKeys;
  }

  return pressedKeys[0].code;
}

interface KeyboardEventHandlers {
  event: React.KeyboardEvent;
  postNewListItem: () => void;
  setShowNewItem: React.Dispatch<React.SetStateAction<boolean>>;
  setIsSubItem: React.Dispatch<React.SetStateAction<boolean>>;
  toggleItemActive?: () => void;
  handleDeleteItem?: () => void;
  setShowMenu?: React.Dispatch<React.SetStateAction<boolean>>;
  setShowManageTag?: React.Dispatch<React.SetStateAction<boolean>>;
  setIsEditing?: React.Dispatch<React.SetStateAction<boolean>>;
}

export const handleKeyPress = ({
  event,
  toggleItemActive,
  setShowNewItem,
  setShowMenu,
  setIsSubItem,
  postNewListItem,
  handleDeleteItem,
  setIsEditing,
  setShowManageTag,
}: KeyboardEventHandlers) => {
  const { code, shiftKey, target } = event;

  const keyPressed = getKeyPress(code);

  const isSpace = code === 'Space';
  const isEnter = code === 'Enter';
  const isEscape = code === 'Escape';
  const isDelete = code === 'Delete';
  const isShift = shiftKey;
  const isTargetDiv = target instanceof HTMLDivElement;
  const isTargetTextArea = target instanceof HTMLTextAreaElement;

  if (isSpace && toggleItemActive) {
    toggleItemActive();
  }
  if (isDelete) {
    handleDeleteItem && handleDeleteItem();
  }

  if (isEnter && isTargetDiv) {
    if (isShift) {
      setIsSubItem(true);
      event.preventDefault();
      setShowNewItem(true);
      return;
    }
    event.preventDefault();
    setShowNewItem(true);
  }

  if (isEnter && isTargetTextArea && !isShift) {
    event.preventDefault();
    postNewListItem();
  }

  if (isEscape) {
    setIsSubItem(false);
    setShowNewItem(false);
  }

  if (code === 'F2' || keyPressed === 'KeyE_KeyE') {
    setIsEditing && setIsEditing(true);
  }

  if (keyPressed === 'KeyT_KeyT') {
    setShowMenu && setShowMenu(true);
    setShowManageTag && setShowManageTag(true);
  }
};
