export function formatDateToLocale(dateTime: Date | string): string {
  if (dateTime === '') return '';
  if (typeof dateTime === 'string') dateTime = new Date(dateTime);
  const userTimeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
  const options: Intl.DateTimeFormatOptions = {
    day: 'numeric',
    month: 'short',
    hour: 'numeric',
    minute: 'numeric',
    timeZone: userTimeZone,
  };
  const formattedDate = dateTime.toLocaleDateString(
    navigator.language,
    options,
  );

  return formattedDate.replace('.,', '');
}

export function parseDateStringToLocale(utcDateString: string): string {
  if (utcDateString === '') return '';

  const userTimeZoneOffset = new Date().getTimezoneOffset();
  const offsetDifference = userTimeZoneOffset * 60000;

  const utcDate = new Date(utcDateString);
  const localDate = new Date(utcDate.getTime() - offsetDifference);
  const localDateString = localDate.toISOString();

  return localDateString;
}
