export function getContrastColor(hexColor: string): string {
  try {
    const hex = hexColor.replace('#', '');

    const red = parseInt(hex.substring(0, 2), 16);
    const green = parseInt(hex.substring(2, 4), 16);
    const blue = parseInt(hex.substring(4, 6), 16);

    const brightness = (red * 299 + green * 587 + blue * 114) / 1000;

    const contrastColor = brightness > 128 ? '#000000' : '#FFFFFF'; // Черный для светлых фонов, белый для темных

    return contrastColor;
  } catch {
    return '#000000';
  }
}
